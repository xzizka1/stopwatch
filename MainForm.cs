﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics; //Stopwatch
using System.Data.SqlClient;
using System.Configuration;

namespace StopwatchWithHistory
{
    public partial class MainForm : Form
    {
        private Stopwatch stopwatch;
        public MainForm()
        {
            InitializeComponent();
            fillGridViewWithTimes(this.dataGridView);
            this.Text = "Stopky";
            stopwatch = new Stopwatch();
        }

        /// <summary>
        /// Button stop the clock when it is running and start when it is not
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void startStopBtn_Click(object sender, EventArgs e)
        {
            if (stopwatch.IsRunning)
                stopwatch.Stop();
            else
                stopwatch.Start();
        }

        /// <summary>
        /// Refreshes the clock in real time and the text on the Start/Stop button 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timer1_Tick(object sender, EventArgs e)
        {
            timeStopwatchLbl.Text = string.Format("{0:h\\:mm\\:ss\\.f}", stopwatch.Elapsed);

            
            if (stopwatch.IsRunning)
                startStopBtn.Text = "Stop";
            else
                startStopBtn.Text = "Start";
        }

        /// <summary>
        /// Sets the clock to zero, but keeps it running
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void restartStopwatchBtn_Click(object sender, EventArgs e)
        {
            stopwatch.Restart();
        }

        /// <summary>
        /// Stops the clock and sets it to zero
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void resetStopwatchBtn_Click(object sender, EventArgs e)
        {
            stopwatch.Reset();
        }

        private void fillGridViewWithTimes(DataGridView dataGV)
        {
            string connectionStr = System.Configuration.ConfigurationManager.ConnectionStrings["StopwatchDB"].ToString();

            using (SqlConnection sqlCon = new SqlConnection(connectionStr))
            {
                sqlCon.Open();
                SqlDataAdapter sqlData = new SqlDataAdapter("SELECT name, time FROM Time", sqlCon);
                DataTable dataTable = new DataTable();
                sqlData.Fill(dataTable);
                dataGV.DataSource = dataTable;
                dataGV.Columns[0].HeaderText = "Jméno";
                dataGV.Columns[1].HeaderText = "Čas";
            }

        }

        /// <summary>
        /// Inserts time to Stopwatch DB 
        /// </summary>
       private void insertTimeToDB(string name, TimeSpan time)
        {
            string connectionStr = System.Configuration.ConfigurationManager.ConnectionStrings["StopwatchDB"].ToString();
            
            using (SqlConnection sqlCon = new SqlConnection(connectionStr))
            {
                sqlCon.Open();
    
                SqlCommand query = new SqlCommand("INSERT INTO Time(name, time) VALUES(@name,@time)", sqlCon);
                query.Parameters.AddWithValue("@name", name);
                query.Parameters.AddWithValue("@time", time);
    
                Debug.Assert(query.ExecuteNonQuery() == 1); // 1 row should be added
            }
        }

        private void saveTimeBtn_Click(object sender, EventArgs e)
        {
            stopwatch.Stop();

            SaveDialog saveDialog = new SaveDialog();
            var save_time = saveDialog.ShowDialog();
            if (save_time == DialogResult.Cancel)
                return;

            string name = saveDialog.Name;

            insertTimeToDB(name, stopwatch.Elapsed);
            fillGridViewWithTimes(this.dataGridView);
           
            MessageBox.Show("Čas byl uložen jako: " + name);   
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            

        }
    }
}
