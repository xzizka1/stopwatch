﻿
namespace StopwatchWithHistory
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.startStopBtn = new System.Windows.Forms.Button();
            this.timeStopwatchLbl = new System.Windows.Forms.Label();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.saveTimeBtn = new System.Windows.Forms.Button();
            this.restartStopwatchBtn = new System.Windows.Forms.Button();
            this.resetStopwatchBtn = new System.Windows.Forms.Button();
            this.stopwatchDBDataSet = new StopwatchWithHistory.StopwatchDBDataSet();
            this.dataGridView = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.stopwatchDBDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // startStopBtn
            // 
            this.startStopBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 25.25F);
            this.startStopBtn.Location = new System.Drawing.Point(48, 178);
            this.startStopBtn.Name = "startStopBtn";
            this.startStopBtn.Size = new System.Drawing.Size(180, 50);
            this.startStopBtn.TabIndex = 0;
            this.startStopBtn.Text = "Start";
            this.startStopBtn.UseVisualStyleBackColor = true;
            this.startStopBtn.Click += new System.EventHandler(this.startStopBtn_Click);
            // 
            // timeStopwatchLbl
            // 
            this.timeStopwatchLbl.AutoSize = true;
            this.timeStopwatchLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 68.25F);
            this.timeStopwatchLbl.Location = new System.Drawing.Point(31, 40);
            this.timeStopwatchLbl.Name = "timeStopwatchLbl";
            this.timeStopwatchLbl.Size = new System.Drawing.Size(424, 102);
            this.timeStopwatchLbl.TabIndex = 3;
            this.timeStopwatchLbl.Text = "0:00:00.0";
            // 
            // timer
            // 
            this.timer.Enabled = true;
            this.timer.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // saveTimeBtn
            // 
            this.saveTimeBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 25.25F);
            this.saveTimeBtn.Location = new System.Drawing.Point(48, 234);
            this.saveTimeBtn.Name = "saveTimeBtn";
            this.saveTimeBtn.Size = new System.Drawing.Size(180, 50);
            this.saveTimeBtn.TabIndex = 6;
            this.saveTimeBtn.Text = "Uložit";
            this.saveTimeBtn.UseVisualStyleBackColor = true;
            this.saveTimeBtn.Click += new System.EventHandler(this.saveTimeBtn_Click);
            // 
            // restartStopwatchBtn
            // 
            this.restartStopwatchBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 25.25F);
            this.restartStopwatchBtn.Location = new System.Drawing.Point(246, 178);
            this.restartStopwatchBtn.Name = "restartStopwatchBtn";
            this.restartStopwatchBtn.Size = new System.Drawing.Size(180, 50);
            this.restartStopwatchBtn.TabIndex = 7;
            this.restartStopwatchBtn.Text = "Restart";
            this.restartStopwatchBtn.UseVisualStyleBackColor = true;
            this.restartStopwatchBtn.Click += new System.EventHandler(this.restartStopwatchBtn_Click);
            // 
            // resetStopwatchBtn
            // 
            this.resetStopwatchBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 25.25F);
            this.resetStopwatchBtn.Location = new System.Drawing.Point(246, 234);
            this.resetStopwatchBtn.Name = "resetStopwatchBtn";
            this.resetStopwatchBtn.Size = new System.Drawing.Size(180, 50);
            this.resetStopwatchBtn.TabIndex = 8;
            this.resetStopwatchBtn.Text = "Reset";
            this.resetStopwatchBtn.UseVisualStyleBackColor = true;
            this.resetStopwatchBtn.Click += new System.EventHandler(this.resetStopwatchBtn_Click);
            // 
            // stopwatchDBDataSet
            // 
            this.stopwatchDBDataSet.DataSetName = "StopwatchDBDataSet";
            this.stopwatchDBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // dataGridView
            // 
            this.dataGridView.AllowUserToAddRows = false;
            this.dataGridView.AllowUserToDeleteRows = false;
            this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView.Location = new System.Drawing.Point(48, 319);
            this.dataGridView.Name = "dataGridView";
            this.dataGridView.ReadOnly = true;
            this.dataGridView.Size = new System.Drawing.Size(378, 304);
            this.dataGridView.TabIndex = 9;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(490, 635);
            this.Controls.Add(this.dataGridView);
            this.Controls.Add(this.resetStopwatchBtn);
            this.Controls.Add(this.restartStopwatchBtn);
            this.Controls.Add(this.saveTimeBtn);
            this.Controls.Add(this.timeStopwatchLbl);
            this.Controls.Add(this.startStopBtn);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.stopwatchDBDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button startStopBtn;
        private System.Windows.Forms.Label timeStopwatchLbl;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.Button saveTimeBtn;
        private System.Windows.Forms.Button restartStopwatchBtn;
        private System.Windows.Forms.Button resetStopwatchBtn;
        private StopwatchDBDataSet stopwatchDBDataSet;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn timeDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridView dataGridView;
    }
}

